# Página Web de Nuevos Juegos - Actividad de Diseño de Interfaces Web

¡Bienvenidos a la Página Web de Nuevos Juegos!

## Introducción

Esta página forma parte de una actividad de clase en el contexto del curso de Diseño de Interfaces Web. La finalidad de esta actividad es explorar y mejorar el concepto de accesibilidad en el diseño web. Durante esta actividad, nos centraremos en optimizar la experiencia de usuarios con diversas capacidades para asegurarnos de que todos puedan disfrutar y navegar por la página de manera efectiva.

## Objetivo

El objetivo principal de esta actividad es aplicar principios de accesibilidad web para mejorar la usabilidad y el acceso a la información en la Página Web de Nuevos Juegos. Algunas de las pautas que se seguirán incluyen:

- **Etiquetas Semánticas:** Utilizaremos etiquetas HTML semánticas para estructurar adecuadamente el contenido y brindar un significado claro a las partes de la página.

- **Contraste Adecuado:** Aseguraremos que los colores de fondo y de texto tengan un contraste suficiente para que el contenido sea legible por todas las personas, incluyendo aquellas con dificultades visuales.

- **Textos Alternativos:** Se añadirán textos alternativos a las imágenes y elementos visuales para describir su contenido a aquellos que dependen de lectores de pantalla.

- **Navegación Simplificada:** Mejoraremos la navegación para que sea fácil de usar con teclado y otros dispositivos de asistencia, permitiendo que los usuarios se desplacen por la página de manera eficiente.

- **Transcripción del Video:** Proporcionaremos una transcripción del video para que las personas con discapacidad auditiva puedan acceder al contenido del mismo.

- **Tamaños de Texto Ajustables:** Aseguraremos que el texto sea escalable, permitiendo a los usuarios ajustar el tamaño según sus necesidades.

## Contribución y Evaluación

Los estudiantes serán invitados a participar activamente en la mejora de la accesibilidad de la página web. Se evaluará la implementación adecuada de las pautas de accesibilidad, así como la comprensión de los principios detrás de ellas. Esta actividad no solo busca la mejora técnica, sino también la concienciación sobre la importancia de la accesibilidad en el diseño web moderno.

Esperamos que esta actividad proporcione una experiencia educativa enriquecedora al tiempo que fomenta la creación de interfaces web más inclusivas y accesibles.
